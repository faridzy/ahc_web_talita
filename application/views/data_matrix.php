<div class="row">
    <!-- Dark table start -->
    <div class="col-12 mt-5">
        <div class="card">
            <div class="card-body">
                <div class="data-tables datatable-dark">
                    <table  class="table table-bordered table-responsive">
                        <thead class="text-capitalize">
                        <tr>
                            <?php for($i=0;$i<=107;$i++){
                                ?>
                                <th><?php echo $i ?></th>
                            <?php
                            } ?>

                        </tr>
                        </thead>
                        <tbody>

                        <?php
                        $no = 1;
                        for($i=1;$i<=107;$i++){
                            ?>
                            <tr>
                                <td><?php echo $no++ ?></td>
                                <?php for($x=1;$x<=107;$x++){
                                    ?>
                                    <td>
                                        <?php
                                        if($i==$x){
                                            echo "<b style='color: #dc3545'>0</b>";
                                        }else{?>
                                            <?php
                                            ?>

                                        <div class="distance_<?php echo ($i-1) ?>_<?php echo ($x-1) ?>"></div>

                                        <?php

                                        }

                                        ?></td>

                                    <?php
                                } ?>
                            </tr>
                            <?php
                        } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- Dark table end -->
</div>
<script src="<?php echo base_url() ?>assets/main/js/vendor/jquery-2.2.4.min.js"></script>
<script>

    <?php
        for($i=0;$i<count($matrix);$i++){
            $distance=explode('_',$matrix[$i]['distance']);
            ?>

        $(".distance_<?php echo $distance[1]?>_<?php echo $distance[2]?>").text(<?php echo $distance[0]?>);

    <?php
        }
    ?>

    <?php
    for($i=0;$i<count($matrix2);$i++){
    $distance=explode('_',$matrix2[$i]['distance']);
    ?>

    $(".distance_<?php echo $distance[1]?>_<?php echo $distance[2]?>").text(<?php echo $distance[0]?>);

    <?php
    }
    ?>

</script>


