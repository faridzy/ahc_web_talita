<div class="row">
    <!-- Dark table start -->
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="data-tables datatable-dark">
                    <table class="table table-responsive table-bordered">
                        <thead class="text-capitalize">
                        <tr>
                            <th>No.</th>
                            <th>Cluster 1</th>
                            <th>Cluster 2</th>
                            <th>Cluster 3</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no=1;
                        ?>


                            <tr>
                                <td><?php echo $no++ ?></td>
                                <?php
                                foreach ($matrix as $key =>$value) {
                                ?>
                                <td><?php echo json_encode($value)?></td>
                                    <?php
                                }
                                ?>
                            </tr>



                        </tbody>
                    </table>
                    <?php

                    if($type==1){
                        //ini buat average data average
                     ?>
                    <br>
                        <br>
                        <h5 align="center">Aspek Suami</h5>
                    <table class="table  table-bordered">
                        <thead class="text-capitalize">
                        <tr>
                            <th>Respon</th>
                            <th>Cluster 1</th>
                            <th>Cluster 2</th>
                            <th>Cluster 3</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Tidak Marah</td>
                            <td>37,5%</td>
                            <td>12,5%</td>
                            <td>13,3%</td>
                        </tr>
                        <tr>
                            <td>Sedikit Marah</td>
                            <td>62,5%</td>
                            <td>12,5%</td>
                            <td>20%</td>
                        </tr>
                        <tr>
                            <td>Agak Marah</td>
                            <td>0%</td>
                            <td>31,2%</td>
                            <td>20,8%</td>
                        </tr>
                        <tr>
                            <td>Marah</td>
                            <td>0%</td>
                            <td>25%</td>
                            <td>27%</td>
                        </tr>
                        <tr>
                            <td>Marah Sekali</td>
                            <td>0%</td>
                            <td>0%</td>
                            <td>17,8%</td>
                        </tr>
                        </tbody>
                    </table>
                        <br>
                        <br>
                        <h5 align="center">Aspek Anak</h5>
                        <table class="table  table-bordered">
                            <thead class="text-capitalize">
                            <tr>
                                <th>Respon</th>
                                <th>Cluster 1</th>
                                <th>Cluster 2</th>
                                <th>Cluster 3</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Tidak Marah</td>
                                <td>0%</td>
                                <td>15%</td>
                                <td>10,9%</td>
                            </tr>
                            <tr>
                                <td>Sedikit Marah</td>
                                <td>0%</td>
                                <td>35%</td>
                                <td>21,4%</td>
                            </tr>
                            <tr>
                                <td>Agak Marah</td>
                                <td>20%</td>
                                <td>20%</td>
                                <td>22,8%</td>
                            </tr>
                            <tr>
                                <td>Marah</td>
                                <td>40%</td>
                                <td>30%</td>
                                <td>25,4%</td>
                            </tr>
                            <tr>
                                <td>Marah Sekali</td>
                                <td>40%</td>
                                <td>0%</td>
                                <td>17,68%</td>
                            </tr>
                            </tbody>
                        </table>
                        <br>
                        <br>
                        <h5 align="center">Aspek Orang tua</h5>
                        <table class="table  table-bordered">
                            <thead class="text-capitalize">
                            <tr>
                                <th>Respon</th>
                                <th>Cluster 1</th>
                                <th>Cluster 2</th>
                                <th>Cluster 3</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Tidak Marah</td>
                                <td>50%</td>
                                <td>30%</td>
                                <td>19,4%</td>
                            </tr>
                            <tr>
                                <td>Sedikit Marah</td>
                                <td>50%</td>
                                <td>45%</td>
                                <td>16,6%</td>
                            </tr>
                            <tr>
                                <td>Agak Marah</td>
                                <td>0%</td>
                                <td>15%</td>
                                <td>24%</td>
                            </tr>
                            <tr>
                                <td>Marah</td>
                                <td>0%</td>
                                <td>10%</td>
                                <td>21,64%</td>
                            </tr>
                            <tr>
                                <td>Marah Sekali</td>
                                <td>0%</td>
                                <td>0%</td>
                                <td>5,7%</td>
                            </tr>
                            </tbody>
                        </table>
                        <br>
                        <br>
                        <h5 align="center">Aspek Saudara</h5>
                        <table class="table  table-bordered">
                            <thead class="text-capitalize">
                            <tr>
                                <th>Respon</th>
                                <th>Cluster 1</th>
                                <th>Cluster 2</th>
                                <th>Cluster 3</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Tidak Marah</td>
                                <td>27%</td>
                                <td>4,54%</td>
                                <td>11,1%</td>
                            </tr>
                            <tr>
                                <td>Sedikit Marah</td>
                                <td>36%</td>
                                <td>13,63%</td>
                                <td>18%</td>
                            </tr>
                            <tr>
                                <td>Agak Marah</td>
                                <td>1%</td>
                                <td>59%</td>
                                <td>23,3%</td>
                            </tr>
                            <tr>
                                <td>Marah</td>
                                <td>0%</td>
                                <td>22,7%</td>
                                <td>25,3%</td>
                            </tr>
                            <tr>
                                <td>Marah Sekali</td>
                                <td>0%</td>
                                <td>0%</td>
                                <td>2%</td>
                            </tr>
                            </tbody>
                        </table>
                        <h5 align="center">Aspek Pekerjaan</h5>
                        <table class="table  table-bordered">
                            <thead class="text-capitalize">
                            <tr>
                                <th>Respon</th>
                                <th>Cluster 1</th>
                                <th>Cluster 2</th>
                                <th>Cluster 3</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Tidak Marah</td>
                                <td>50%</td>
                                <td>25%</td>
                                <td>11,3%</td>
                            </tr>
                            <tr>
                                <td>Sedikit Marah</td>
                                <td>50%</td>
                                <td>40%</td>
                                <td>22%</td>
                            </tr>
                            <tr>
                                <td>Agak Marah</td>
                                <td>0%</td>
                                <td>15%</td>
                                <td>27,7%</td>
                            </tr>
                            <tr>
                                <td>Marah</td>
                                <td>0%</td>
                                <td>20%</td>
                                <td>26,4%</td>
                            </tr>
                            <tr>
                                <td>Marah Sekali</td>
                                <td>0%</td>
                                <td>0%</td>
                                <td>12,1%</td>
                            </tr>
                            </tbody>
                        </table>
                        <br>
                        <br>

                    <?php
                    }elseif ($type==2){

                        //ini untuk single
                        ?>

                        <br>
                        <br>
                        <h5 align="center">Aspek Suami</h5>
                        <table class="table  table-bordered">
                            <thead class="text-capitalize">
                            <tr>
                                <th>Respon</th>
                                <th>Cluster 1</th>
                                <th>Cluster 2</th>
                                <th>Cluster 3</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Tidak Marah</td>
                                <td>25%</td>
                                <td>13,6%</td>
                                <td>12,5%</td>
                            </tr>
                            <tr>
                                <td>Sedikit Marah</td>
                                <td>25%</td>
                                <td>21,5%</td>
                                <td>12,5%</td>
                            </tr>
                            <tr>
                                <td>Agak Marah</td>
                                <td>12,5%</td>
                                <td>20,1%</td>
                                <td>25%</td>
                            </tr>
                            <tr>
                                <td>Marah</td>
                                <td>37,5%</td>
                                <td>25,8%</td>
                                <td>12,5%</td>
                            </tr>
                            <tr>
                                <td>Marah Sekali</td>
                                <td>0%</td>
                                <td>18%</td>
                                <td>50%</td>
                            </tr>
                            </tbody>
                        </table>
                        <br>
                        <br>
                        <h5 align="center">Aspek Anak</h5>
                        <table class="table  table-bordered">
                            <thead class="text-capitalize">
                            <tr>
                                <th>Respon</th>
                                <th>Cluster 1</th>
                                <th>Cluster 2</th>
                                <th>Cluster 3</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Tidak Marah</td>
                                <td>30%</td>
                                <td>10,8%</td>
                                <td>0%</td>
                            </tr>
                            <tr>
                                <td>Sedikit Marah</td>
                                <td>30%</td>
                                <td>21,3%</td>
                                <td>50%</td>
                            </tr>
                            <tr>
                                <td>Agak Marah</td>
                                <td>20%</td>
                                <td>23,3%</td>
                                <td>20%</td>
                            </tr>
                            <tr>
                                <td>Marah</td>
                                <td>20%</td>
                                <td>25,7%</td>
                                <td>30%</td>
                            </tr>
                            <tr>
                                <td>Marah Sekali</td>
                                <td>0%</td>
                                <td>18,9%</td>
                                <td>0%</td>
                            </tr>
                            </tbody>
                        </table>
                        <br>
                        <br>
                        <h5 align="center">Aspek Orang tua</h5>
                        <table class="table  table-bordered">
                            <thead class="text-capitalize">
                            <tr>
                                <th>Respon</th>
                                <th>Cluster 1</th>
                                <th>Cluster 2</th>
                                <th>Cluster 3</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Tidak Marah</td>
                                <td>30%</td>
                                <td>20,2%</td>
                                <td>10%</td>
                            </tr>
                            <tr>
                                <td>Sedikit Marah</td>
                                <td>20%</td>
                                <td>28,6%</td>
                                <td>30%</td>
                            </tr>
                            <tr>
                                <td>Agak Marah</td>
                                <td>30%</td>
                                <td>24,2%</td>
                                <td>30%</td>
                            </tr>
                            <tr>
                                <td>Marah</td>
                                <td>20%</td>
                                <td>20,2%</td>
                                <td>20%</td>
                            </tr>
                            <tr>
                                <td>Marah Sekali</td>
                                <td>0%</td>
                                <td>61,3%</td>
                                <td>10%</td>
                            </tr>
                            </tbody>
                        </table>
                        <br>
                        <br>
                        <h5 align="center">Aspek Saudara</h5>
                        <table class="table  table-bordered">
                            <thead class="text-capitalize">
                            <tr>
                                <th>Respon</th>
                                <th>Cluster 1</th>
                                <th>Cluster 2</th>
                                <th>Cluster 3</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Tidak Marah</td>
                                <td>9,09%</td>
                                <td>11,2%</td>
                                <td>36,3%</td>
                            </tr>
                            <tr>
                                <td>Sedikit Marah</td>
                                <td>27,2%</td>
                                <td>18,6%</td>
                                <td>0%</td>
                            </tr>
                            <tr>
                                <td>Agak Marah</td>
                                <td>54,4%</td>
                                <td>23,1%</td>
                                <td>18,8%</td>
                            </tr>
                            <tr>
                                <td>Marah</td>
                                <td>9,09%</td>
                                <td>25,4%</td>
                                <td>36,3%</td>
                            </tr>
                            <tr>
                                <td>Marah Sekali</td>
                                <td>0%</td>
                                <td>20,6%</td>
                                <td>9,09%</td>
                            </tr>
                            </tbody>
                        </table>
                        <h5 align="center">Aspek Pekerjaan</h5>
                        <table class="table  table-bordered">
                            <thead class="text-capitalize">
                            <tr>
                                <th>Respon</th>
                                <th>Cluster 1</th>
                                <th>Cluster 2</th>
                                <th>Cluster 3</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Tidak Marah</td>
                                <td>20%</td>
                                <td>12,1%</td>
                                <td>0%</td>
                            </tr>
                            <tr>
                                <td>Sedikit Marah</td>
                                <td>10%</td>
                                <td>22,1%</td>
                                <td>30%</td>
                            </tr>
                            <tr>
                                <td>Agak Marah</td>
                                <td>30%</td>
                                <td>27,8%</td>
                                <td>30%</td>
                            </tr>
                            <tr>
                                <td>Marah</td>
                                <td>40%</td>
                                <td>25,6%</td>
                                <td>40%</td>
                            </tr>
                            <tr>
                                <td>Marah Sekali</td>
                                <td>0%</td>
                                <td>12,1%</td>
                                <td>0%</td>
                            </tr>
                            </tbody>
                        </table>

                    <?php
                    }elseif ($type==3){
                        //ini complete

                        ?>

                        <br>
                        <br>
                        <h5 align="center">Aspek Suami</h5>
                        <table class="table  table-bordered">
                            <thead class="text-capitalize">
                            <tr>
                                <th>Respon</th>
                                <th>Cluster 1</th>
                                <th>Cluster 2</th>
                                <th>Cluster 3</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Tidak Marah</td>
                                <td>15,6%</td>
                                <td>16,6%</td>
                                <td>12,2%</td>
                            </tr>
                            <tr>
                                <td>Sedikit Marah</td>
                                <td>28,1%</td>
                                <td>14%</td>
                                <td>21,4%</td>
                            </tr>
                            <tr>
                                <td>Agak Marah</td>
                                <td>21,8%</td>
                                <td>13,8%</td>
                                <td>22,3%</td>
                            </tr>
                            <tr>
                                <td>Marah</td>
                                <td>34,3%</td>
                                <td>29,8%</td>
                                <td>25,2%</td>
                            </tr>
                            <tr>
                                <td>Marah Sekali</td>
                                <td>0%</td>
                                <td>20,8%</td>
                                <td>18,4%</td>
                            </tr>
                            </tbody>
                        </table>
                        <br>
                        <br>
                        <h5 align="center">Aspek Anak</h5>
                        <table class="table  table-bordered">
                            <thead class="text-capitalize">
                            <tr>
                                <th>Respon</th>
                                <th>Cluster 1</th>
                                <th>Cluster 2</th>
                                <th>Cluster 3</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Tidak Marah</td>
                                <td>15%</td>
                                <td>10%</td>
                                <td>11,5%</td>
                            </tr>
                            <tr>
                                <td>Sedikit Marah</td>
                                <td>17,5%</td>
                                <td>17,2%</td>
                                <td>22,1%</td>
                            </tr>
                            <tr>
                                <td>Agak Marah</td>
                                <td>25%</td>
                                <td>17,7%</td>
                                <td>25,9%</td>
                            </tr>
                            <tr>
                                <td>Marah</td>
                                <td>30%</td>
                                <td>32,7%</td>
                                <td>24%</td>
                            </tr>
                            <tr>
                                <td>Marah Sekali</td>
                                <td>10%</td>
                                <td>21,6%</td>
                                <td>16,4%</td>
                            </tr>
                            </tbody>
                        </table>
                        <br>
                        <br>
                        <h5 align="center">Aspek Orang tua</h5>
                        <table class="table  table-bordered">
                            <thead class="text-capitalize">
                            <tr>
                                <th>Respon</th>
                                <th>Cluster 1</th>
                                <th>Cluster 2</th>
                                <th>Cluster 3</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Tidak Marah</td>
                                <td>32,5%</td>
                                <td>18,8%</td>
                                <td>20,3%</td>
                            </tr>
                            <tr>
                                <td>Sedikit Marah</td>
                                <td>50%</td>
                                <td>22,2%</td>
                                <td>29,6%</td>
                            </tr>
                            <tr>
                                <td>Agak Marah</td>
                                <td>75%</td>
                                <td>23%</td>
                                <td>25,2%</td>
                            </tr>
                            <tr>
                                <td>Marah</td>
                                <td>10%</td>
                                <td>26,6%</td>
                                <td>19,4%</td>
                            </tr>
                            <tr>
                                <td>Marah Sekali</td>
                                <td>0%</td>
                                <td>88%</td>
                                <td>52,6%</td>
                            </tr>
                            </tbody>
                        </table>
                        <br>
                        <br>
                        <h5 align="center">Aspek Saudara</h5>
                        <table class="table  table-bordered">
                            <thead class="text-capitalize">
                            <tr>
                                <th>Respon</th>
                                <th>Cluster 1</th>
                                <th>Cluster 2</th>
                                <th>Cluster 3</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Tidak Marah</td>
                                <td>9,09%</td>
                                <td>14,1%</td>
                                <td>10,3%</td>
                            </tr>
                            <tr>
                                <td>Sedikit Marah</td>
                                <td>22,7%</td>
                                <td>11,1%</td>
                                <td>20,5%</td>
                            </tr>
                            <tr>
                                <td>Agak Marah</td>
                                <td>56,8%</td>
                                <td>21,2%</td>
                                <td>23,1%</td>
                            </tr>
                            <tr>
                                <td>Marah</td>
                                <td>11,3%</td>
                                <td>33,3%</td>
                                <td>23,6%</td>
                            </tr>
                            <tr>
                                <td>Marah Sekali</td>
                                <td>0%</td>
                                <td>20,2%</td>
                                <td>21,2%</td>
                            </tr>
                            </tbody>
                        </table>
                        <h5 align="center">Aspek Pekerjaan</h5>
                        <table class="table  table-bordered">
                            <thead class="text-capitalize">
                            <tr>
                                <th>Respon</th>
                                <th>Cluster 1</th>
                                <th>Cluster 2</th>
                                <th>Cluster 3</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Tidak Marah</td>
                                <td>25%</td>
                                <td>15,5%</td>
                                <td>10,1%</td>
                            </tr>
                            <tr>
                                <td>Sedikit Marah</td>
                                <td>37,5%</td>
                                <td>11,1%</td>
                                <td>25,9%</td>
                            </tr>
                            <tr>
                                <td>Agak Marah</td>
                                <td>15%</td>
                                <td>24,4%</td>
                                <td>29,2%</td>
                            </tr>
                            <tr>
                                <td>Marah</td>
                                <td>22,5%</td>
                                <td>39,4%</td>
                                <td>21,7%</td>
                            </tr>
                            <tr>
                                <td>Marah Sekali</td>
                                <td>0%</td>
                                <td>9,4%</td>
                                <td>13,1%</td>
                            </tr>
                            </tbody>
                        </table>



                    <?php
                    }
                    ?>
                    <table>

                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- Dark table end -->
</div>