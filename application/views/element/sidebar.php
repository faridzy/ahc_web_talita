<!-- sidebar menu area start -->
<div class="sidebar-menu">
    <div class="sidebar-header">
        <div class="logo">
            <a href="#" style="color: white"><h3><b>Wanita Jawa</b></h3></a>
        </div>
    </div>
    <div class="main-menu">
        <div class="menu-inner">
            <nav>
                <ul class="metismenu" id="menu">
                    <li><a href="<?php echo site_url('Data'); ?>"><i class="ti-notepad"></i> <span>Data Kuesioner</span></a></li>
                    <li><a href="<?php echo site_url('Data/form_data'); ?>"><i class="ti-import"></i> <span>Import Kuesioner</span></a></li>
                    <li><a href="<?php echo site_url('Data/matrix_data'); ?>"><i class="ti-layers-alt"></i> <span>Matrix Euclidean Clustering</span></a></li>
                    <li><a href="<?php echo site_url('Data/getAverageData'); ?>"><i class="ti-filter"></i> <span>Average Clustering</span></a></li>
                    <li><a href="<?php echo site_url('Data/getSingleData'); ?>"><i class="ti-filter"></i> <span>Single Clustering</span></a></li>
                    <li><a href="<?php echo site_url('Data/getCompleteData'); ?>"><i class="ti-filter"></i> <span>Complete Clustering</span></a></li>
                    <li><a href="<?php echo site_url('Data/getClusterCity'); ?>"><i class="ti-filter"></i> <span>Berdasarkan Kota</span></a></li>



                </ul>
            </nav>
        </div>
    </div>
</div>
<!-- sidebar menu area end -->