<div class="row">
    <!-- Dark table start -->
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="data-tables datatable-dark">
                    <h5 align="center">Berdasarkan Kota</h5>
                    <table class="table  table-bordered">
                        <thead class="text-capitalize">
                        <tr>
                            <th>#</th>
                            <th>Cluster 1</th>
                            <th>Cluster 2</th>
                            <th>Cluster 3</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td></td>
                            <td>5	Blitar</td>
                            <td>61	Situbondo
                                7	Blitar</td>
                            <td>
                                79	Lumajang	40	Surabaya
                                78	Lumajang	22	Sidoarjo
                                18	Malang	54	Gresik
                                74	Lumajang	52	Gresik
                                14	Malang	27	Sidoarjo
                                76	Lumajang	67	Situbondo
                                75	Lumajang	20	Malang
                                65	Situbondo	21	Sidoarjo
                                32	Surabaya	2	Blitar
                                30	Sidoarjo	36	Surabaya
                                72	Lumajang	3	Blitar
                                71	Lumajang	10	Blitar
                                38	Surabaya	60	Gresik
                                16	Malang	15	Malang
                                39	Surabaya	19	Malang
                                17	Malang	4	Blitar
                                66	Situbondo	69	Situbondo
                                49	Pasuruan	24	Sidoarjo
                                42	Pasuruan	23	Sidoarjo
                                47	Pasuruan	51	Gresik
                                46	Pasuruan	8	Blitar
                                44	Pasuruan	62	Situbondo
                                45	Pasuruan	9	Blitar
                                48	Pasuruan	33	Surabaya
                                41	Pasuruan	53	Gresik
                                13	Malang	50	Pasuruan
                                59	Gresik	35	Surabaya
                                63	Situbondo	28	Sidoarjo
                                37	Surabaya
                                34	Surabaya
                                73	Lumajang
                                12	Malang
                                43	Pasuruan
                                31	Surabaya
                                77	Lumajang
                                1	Blitar
                                70	Situbondo
                                25	Sidoarjo
                                11	Malang
                                29	Sidoarjo
                                6	Blitar
                                58	Gresik
                                57	Gresik
                                56	Gresik
                                55	Gresik
                                68	Situbondo
                                64	Situbondo
                            </td>
                        </tr>
                        </tbody>
                    </table>


                    <br>
                    <br>
                    <h5 align="center">Berdasarkan Kota</h5>
                    <table class="table  table-bordered">
                        <thead class="text-capitalize">
                        <tr>
                            <th>#</th>
                            <th>Cluster 1</th>
                            <th>Cluster 2</th>
                            <th>Cluster 3</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td></td>
                            <td>

                                61	Situbondo
                            </td>
                            <td>
                                78	Lumajang
                                18	Malang
                                74	Lumajang
                                14	Malang
                                76	Lumajang
                                75	Lumajang
                                72	Lumajang
                                71	Lumajang
                                32	Surabaya
                                30	Sidoarjo
                                65	Situbondo
                                1	Blitar
                                70	Situbondo
                                25	Sidoarjo
                                11	Malang
                                68	Situbondo
                                64	Situbondo
                                40	Surabaya
                                22	Sidoarjo
                                67	Situbondo
                                20	Malang
                                21	Sidoarjo
                                10	Blitar
                                2	Blitar
                                60	Gresik
                                29	Sidoarjo
                                26	Sidoarjo
                                13	Malang
                                36	Surabaya
                                66	Situbondo
                                49	Pasuruan
                                42	Pasuruan
                                47	Pasuruan
                                46	Pasuruan
                                45	Pasuruan
                                41	Pasuruan
                                44	Pasuruan
                                58	Gresik
                                57	Gresik
                                56	Gresik
                                55	Gresik
                                3	Blitar
                                54	Gresik
                                62	Situbondo
                                9	Blitar
                                50	Pasuruan
                                6	Blitar
                                38	Surabaya
                                27	Sidoarjo
                                5	Blitar
                                23	Sidoarjo
                                73	Lumajang
                                12	Malang
                                17	Malang
                                15	Malang
                                19	Malang
                                31	Surabaya
                                28	Sidoarjo
                                39	Surabaya
                                52	Gresik
                                34	Surabaya
                                53	Gresik
                                24	Sidoarjo
                                43	Pasuruan
                                59	Gresik
                                69	Situbondo
                                16	Malang
                                77	Lumajang
                                35	Surabaya
                                51	Gresik
                                63	Situbondo
                                37	Surabaya
                                8	Blitar
                                33	Surabaya
                                47	Pasuruan

                            </td>
                            <td>
                                79	Lumajang
                            </td>
                        </tr>
                        </tbody>
                    </table>

                    <br>
                    <h5 align="center">Berdasarkan Kota</h5>
                    <table class="table  table-bordered">
                        <thead class="text-capitalize">
                        <tr>
                            <th>#</th>
                            <th>Cluster 1</th>
                            <th>Cluster 2</th>
                            <th>Cluster 3</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td></td>
                            <td>
                                61	Situbondo
                                7	Blitar
                                5	Blitar
                                4	Blitar
                            </td>
                            <td>
                                66	Situbondo
                                47	Pasuruan
                                46	Pasuruan
                                49	Pasuruan
                                42	Pasuruan
                                45	Pasuruan
                                48	Pasuruan
                                34	Pasuruan
                                63	Situbondo
                                53	Gresik
                                35	Surabaya
                                50	Pasuruan
                                44	Pasuruan
                                28	Sidoarjo
                                23	Sidoarjo
                                62	Situbondo
                                9	Blitar
                                33	Surabaya
                            </td>
                            <td>
                                79	Lumajang
                                76	Lumajang
                                75	Lumajang
                                2	Bliar
                                1	Bliar
                                78	Lumajang
                                18	Malang
                                77	Lumajang
                                39	Surabaya
                                17	Malang
                                74	Lumajang
                                14	Malang
                                73	Lumajang
                                12	Malang
                                38	Surabaya
                                16	Malang
                                72	Lumajang
                                71	Lumajang
                                37	Surabaya
                                65	Situbondo
                                32	Surabaya
                                30	Sidoarjo
                                59	Gresik
                                67	Situbondo
                                20	Malang
                                21	Sidoarjo
                                36	Surabaya
                                15	Malang
                                60	Gresik
                                19	Malang
                                10	Blitar
                                70	Situbondo
                                25	Sidoarjo
                                11	Malang
                                6	Blitar
                                29	Sidoarjo
                                26	Sidoarjo
                                27	Sidoarjo
                                68	Situbondo
                                64	Situbondo
                                40	Surabaya
                                54	Gresik
                                22	Sidoarjo
                                52	Gresik
                                69	Situbondo
                                41	Pasuruan
                                13	Malang
                                58	Gresik
                                57	Gresik
                                56	Gresik
                                55	Gresik
                                24	Sidoarjo
                                51	Gresik
                                43	Pasuruan
                                31	Surabaya
                                8	Blitar
                                3	Blitar
                            </td>
                        </tr>
                        </tbody>
                    </table>



                </div>
            </div>
        </div>
    </div>
    <!-- Dark table end -->
</div>