<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Cluster extends CI_Controller{

		public function __construct()
		{
			parent::__construct();
			$this->load->model('model_cluster');

		}

		public function index()
		{
			$data['aktif'] = '6';
			$data['data'] = $this->M_cluster->getData();
			$data['datarandom'] = $this->M_cluster->getDataRand();
			$this->load->view('cluster/cluster_view', $data);
		}
		public function lanjut()
		{
			$data['aktif'] = '6';
		$data['datahasil'] = $this->M_knn->getDataHasil();
		$data['datahasilrank'] = $this->M_knn->getDataHasilRank();
		$this->load->view('cluster/cluster_view', $data);
		}

	public function hitung_cluster()
	{
		//mengambil data kuesioner dari database
		$data = $this->model_cluster->getData();

		//step 1 : normalisasi data
		//membuat array dengan komponen setengah komponen tabel
		$x = 1;
		for($z=0; $z<8; $z++){
			$y = $x;
			$arr['norm'][$x][$z+1] = 0;
			for($i=$z; $i<7; $i++){
				$arr['norm'][$x][$y+1] = sqrt( //sqrt = fungsi untuk menghitung nilai akar pangkat 2
								pow($data[$z]['kuisioner1']-$data[$i+1]['kuisioner1'], 2)+ //pow = fungsi untuk menghitung nilai kuadrat
								pow($data[$z]['kuisioner2']-$data[$i+1]['kuisioner2'], 2)+
								pow($data[$z]['kuisioner3']-$data[$i+1]['kuisioner3'], 2)+
								pow($data[$z]['kuisioner4']-$data[$i+1]['kuisioner4'], 2)+
								pow($data[$z]['kuisioner5']-$data[$i+1]['kuisioner5'], 2)+
								pow($data[$z]['kuisioner6']-$data[$i+1]['kuisioner6'], 2)+
								pow($data[$z]['kuisioner7']-$data[$i+1]['kuisioner7'], 2)+
								pow($data[$z]['kuisioner8']-$data[$i+1]['kuisioner8'], 2)
								// pow($data[$i]['kuisioner9']-$data[$i+1]['kuisioner9'], 2)+
								// pow($data[$i]['kuisioner10']-$data[$i+1]['kuisioner10'], 2)+
								// pow($data[$i]['kuisioner11']-$data[$i+1]['kuisioner11'], 2)+
								// pow($data[$i]['kuisioner12']-$data[$i+1]['kuisioner12'], 2)+
								// pow($data[$i]['kuisioner13']-$data[$i+1]['kuisioner13'], 2)+
								// pow($data[$i]['kuisioner14']-$data[$i+1]['kuisioner14'], 2)+
								// pow($data[$i]['kuisioner15']-$data[$i+1]['kuisioner15'], 2)+
								// pow($data[$i]['kuisioner16']-$data[$i+1]['kuisioner16'], 2)+
								// pow($data[$i]['kuisioner17']-$data[$i+1]['kuisioner17'], 2)+
								// pow($data[$i]['kuisioner18']-$data[$i+1]['kuisioner18'], 2)+
								// pow($data[$i]['kuisioner19']-$data[$i+1]['kuisioner19'], 2)+
								// pow($data[$i]['kuisioner20']-$data[$i+1]['kuisioner20'], 2)+
								// pow($data[$i]['kuisioner21']-$data[$i+1]['kuisioner21'], 2)+
								// pow($data[$i]['kuisioner22']-$data[$i+1]['kuisioner22'], 2)+
								// pow($data[$i]['kuisioner23']-$data[$i+1]['kuisioner23'], 2)+
								// pow($data[$i]['kuisioner24']-$data[$i+1]['kuisioner24'], 2)+
								// pow($data[$i]['kuisioner25']-$data[$i+1]['kuisioner25'], 2)+
								// pow($data[$i]['kuisioner26']-$data[$i+1]['kuisioner26'], 2)+
								// pow($data[$i]['kuisioner27']-$data[$i+1]['kuisioner27'], 2)+
								// pow($data[$i]['kuisioner28']-$data[$i+1]['kuisioner28'], 2)+
								// pow($data[$i]['kuisioner29']-$data[$i+1]['kuisioner29'], 2)+
								// pow($data[$i]['kuisioner30']-$data[$i+1]['kuisioner30'], 2)+
								// pow($data[$i]['kuisioner31']-$data[$i+1]['kuisioner31'], 2)+
								// pow($data[$i]['kuisioner32']-$data[$i+1]['kuisioner32'], 2)+
								// pow($data[$i]['kuisioner33']-$data[$i+1]['kuisioner33'], 2)+
								// pow($data[$i]['kuisioner34']-$data[$i+1]['kuisioner34'], 2)+
								// pow($data[$i]['kuisioner35']-$data[$i+1]['kuisioner35'], 2)+
								// pow($data[$i]['kuisioner36']-$data[$i+1]['kuisioner36'], 2)+
								// pow($data[$i]['kuisioner37']-$data[$i+1]['kuisioner37'], 2)+
								// pow($data[$i]['kuisioner38']-$data[$i+1]['kuisioner38'], 2)+
								// pow($data[$i]['kuisioner39']-$data[$i+1]['kuisioner39'], 2)+
								// pow($data[$i]['kuisioner40']-$data[$i+1]['kuisioner40'], 2)+
								// pow($data[$i]['kuisioner41']-$data[$i+1]['kuisioner41'], 2)+
								// pow($data[$i]['kuisioner42']-$data[$i+1]['kuisioner42'], 2)+
								// pow($data[$i]['kuisioner43']-$data[$i+1]['kuisioner43'], 2)+
								// pow($data[$i]['kuisioner44']-$data[$i+1]['kuisioner44'], 2)+
								// pow($data[$i]['kuisioner45']-$data[$i+1]['kuisioner45'], 2)+
								// pow($data[$i]['kuisioner46']-$data[$i+1]['kuisioner46'], 2)+
								// pow($data[$i]['kuisioner47']-$data[$i+1]['kuisioner47'], 2)+
								// pow($data[$i]['kuisioner48']-$data[$i+1]['kuisioner48'], 2)+
								// pow($data[$i]['kuisioner49']-$data[$i+1]['kuisioner49'], 2)
							);
				$y++;
			}
			// $arr['number'][$x] = $x;
			$x++;
		}
		$arr2 = $arr['norm']; // memasukkan array data hasil normalisasi ke variabel lain untuk dibuat perhitungan

		//membuat array dengan komponen keseluruhan tabel
		$a = 1;
		for($q=0; $q<8; $q++){
			$b = 1;
			for($r=0; $r<8; $r++){
					$arr['norm1'][$a][$b] = sqrt(
									pow($data[$q]['kuisioner1']-$data[$r]['kuisioner1'], 2)+
									pow($data[$q]['kuisioner2']-$data[$r]['kuisioner2'], 2)+
									pow($data[$q]['kuisioner3']-$data[$r]['kuisioner3'], 2)+
									pow($data[$q]['kuisioner4']-$data[$r]['kuisioner4'], 2)+
									pow($data[$q]['kuisioner5']-$data[$r]['kuisioner5'], 2)+
									pow($data[$q]['kuisioner6']-$data[$r]['kuisioner6'], 2)+
									pow($data[$q]['kuisioner7']-$data[$r]['kuisioner7'], 2)+
									pow($data[$q]['kuisioner8']-$data[$r]['kuisioner8'], 2)
								);
				$b++;
			}
			$a++;
		}

		//perulangan untuk membuat array dengan komposisi vertikal
		for ($i=1; $i <= 8; $i++) { 
			for ($b=1; $b <= $i; $b++) { 
				$arr['norm_ver'][$i][$b] = $arr2[$b][$i];
			}
			$bu = array_search(0, $arr['norm_ver'][$i]);
			unset($arr['norm_ver'][$i][$bu]);
			if($arr['norm_ver'][$i]==null){
				unset($arr['norm_ver'][$i]);
			}
		}

		echo "==============NORM VER=================<br>";
		echo json_encode($arr['norm_ver']);
		echo "<br>===============================<br>";
		
		//mengambil niali minimal dari semua kolom
		$minValueOnColumn_normver = array();
		//perulangan dimulai dr indeks prtm array norm ver trs diulng smp 8 kli
		for ($i=key($arr['norm_ver']); $i <= 8; $i++) { 
			//jumlh dta di array $si pd arry norm ver lbh dr 0
			if (count($arr['norm_ver'][$i])!=0) {
			//mka akn ngisi arry $minvalue on coloum $i id dg kolom trkcl cnth 1-4 atw 2-4
				$minValueOnColumn_normver[$i]['id'] = $i."-".array_search(min($arr['norm_ver'][$i]), $arr['norm_ver'][$i]);
				//ngsi nilai kclny dr klom yg trkcl
				$minValueOnColumn_normver[$i]['value'] = min($arr['norm_ver'][$i]);
			}
		}

		//untk mensortir array, nilai kecil biar didepan
		array_multisort(array_column($minValueOnColumn_normver, "value"), SORT_ASC, $minValueOnColumn_normver);
		// print_r($minValueOnColumn_normver);

		//nmpilin 2 trdpn dr arry $minclmn, jd keambl 2 nlai trkcl
		echo "terendah 1 :".$minValueOnColumn_normver[0]['value']."<br>";//isi kolom
		echo "terendah 2 :".$minValueOnColumn_normver[1]['value']."<br>";
		echo "id 1 :".$minValueOnColumn_normver[0]['id']."<br>";//tmpny where
		echo "id 2 :".$minValueOnColumn_normver[1]['id']."<br>";
		echo "<br><br>";

		//contoh ambil data
		$id1 = explode("-", $minValueOnColumn_normver[0]['id']);//untk pemisas string yg mau d ambl kan cm angkanya, kan array 4 2 
		//itu yg mau d ambl angkny doang, pemisahnya "_-"
		$id2 = explode("-", $minValueOnColumn_normver[1]['id']);
		echo "contoh ambil data terendah 1 : ".$arr['norm_ver'][$id1[0]][$id1[1]];

		//perulangan tuk perbandgn min () ke stiap kolom yng tidak diambl sbg kolom yg memiliki nili kcl
		for ($i=1; $i <= count($arr['norm1']); $i++) { //perulangan smp (jmlh dt array)
			if(key($arr['norm1'][$i])!=$id1[0] || key($arr['norm1'][$i])!=$id2[0]) {//jika nilai indeks dr arry norm tidak sm dg indek dt trkcl mk akan 
				//dllkukan prbndngn min

				$arr['norm'][$id1[0].$id2[0]][$i] = min($arr['norm1'][$id1[0]][$i], $arr['norm1'][$id2[0]][$i]);
				
			}
		}

		echo "<br><br>";
		echo json_encode($arr['norm']);
		
	}

	// function hitung_metode()
	// {
	// 	$dummy[0][0] = 1;
		
	// 	$dummy[1][0] = 2;
	// 	$dummy[1][1] = 0.3;

	// 	$dummy[2][0] = 3.14;
	// 	$dummy[2][1] = 1.15;
	// 	$dummy[2][2] = 0.4;

	// 	$dummy[3][0] = 5.45;
	// 	$dummy[3][1] = 2.28;
	// 	$dummy[3][2] = 3;
	// 	$dummy[3][3] = 0.5;

	// 	$dummy[4][0] = 1.14;
	// 	$dummy[4][1] = 8;
	// 	$dummy[4][2] = 5.45;
	// 	$dummy[4][3] = 8;
	// 	$dummy[4][4] = 0.2;
	// 	echo "data dummy  <br><br>";
	// 	print_r		($dummy);
	// 	echo "<br><br>";
	// 	$minValueOnColumn = array();
	// 	for ($i=0; $i < count($dummy); $i++) { 
	// 		$minValueOnColumn[$i]['id'] = $i."-".array_search(min($dummy[$i]), $dummy[$i]);
	// 		$minValueOnColumn[$i]['value'] = min($dummy[$i]);
	// 	}
	// 	print_r($minValueOnColumn);
	// 	echo "<br><br>";
	// 	array_multisort(array_column($minValueOnColumn, "value"), SORT_ASC, $minValueOnColumn);
	// 	print_r($minValueOnColumn);
	// 	echo "<br>";
	// 	echo "terencah 1 :".$minValueOnColumn[0]['value']."<br>";
	// 	echo "terencah 2 :".$minValueOnColumn[1]['value']."<br>";
	// 	echo "id 1 :".$minValueOnColumn[0]['id']."<br>";
	// 	echo "id 2 :".$minValueOnColumn[1]['id']."<br>";

	// }
 }
